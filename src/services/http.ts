import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}

instance.interceptors.response.use(
  function (res) {
    return delay(1).then(() => res)
  },
  function (error) {
    return Promise.reject(error)
  }
)

export default instance
